from django.apps import AppConfig


class BeerRecoConfig(AppConfig):
    name = 'beer_reco'
