from rest_framework import routers

from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from beer_reco import views

urlpatterns = [
    path('api/beersbyrating/', views.BeersByRating),
    path('api/beersbypopularity/', views.BeersByPopularity),
    path('api/beersbyrecommendation/', views.BeersByrecommendation),
    path('api/userpreferences/', views.UserPreferences)
]

urlpatterns = format_suffix_patterns(urlpatterns)
