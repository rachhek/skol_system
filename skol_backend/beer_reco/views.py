# Create your views here.
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import pickle
import json

from collections import defaultdict
from math import sqrt
from sklearn.metrics import mean_squared_error
from sklearn import preprocessing
from surprise.model_selection import train_test_split
from surprise import accuracy
from surprise.accuracy import rmse
from surprise import SVD
from surprise import KNNBaseline
from surprise.model_selection import cross_validate
from surprise import Dataset
from surprise import Reader
from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.conf import settings
from django.http import JsonResponse

# RMSE of the top-N recommendation

# beer_reviews = pd.read_csv("exported.csv")
beer_reviews = pd.read_csv(settings.CSV_FILE)
# os.path.join(BASE_DIR, 'db.sqlite3')
beer_data = pd.read_pickle(settings.BASELINE_PKL)


formatted_ratings = ""
formatted_criteriaOrdering = ""


def makeData(df, review):
    beer_overall = df[['beer_beerid', 'review_profilename', review]]
    reader = Reader(rating_scale=(0, 5))
    data = Dataset.load_from_df(
        beer_overall[['review_profilename', 'beer_beerid', review]], reader)
    return data


def top20beer(beer_data, op):
    result = []

    topreview = [70054, 63649, 35787, 44910, 54147, 17210, 56266, 67111, 29236,
                 65686, 59949,  8626, 24273, 68548, 37070, 47503, 61884, 25211,
                 60204, 59147]
    toppop = [2093,   412,  1904,  1093,    92,  4083,   276,    88,  7971,
              11757,  2671,    34,  6108,  1013,   695,   680, 17112,   104,
              1160,  1005]
    tops = []
    if op == 'score':
        tops = topreview
    elif op == 'popularity':
        tops = toppop

    for idx in tops:
        tmp = beer_reviews[beer_reviews['beer_beerid'] == idx][['beer_beerid', 'beer_name',
                                                                'beer_abv', 'country_name', 'beer_style', 'brewery_name']].drop_duplicates().fillna(0).values

        result.append({
            'id': tmp[0][0],
            'name': tmp[0][1],
            'abv': tmp[0][2],
            'country': tmp[0][3],
            'style': tmp[0][4],
            'brewery': tmp[0][5]
        })
    return result


@api_view(['GET'])
def BeersByRating(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        # snippets = Snippet.objects.all()
        # serializer = SnippetSerializer(snippets, many=True)
        print(top20beer(beer_data, 'score'))
        return Response(top20beer(beer_data, 'score'))


@api_view(['GET'])
def BeersByPopularity(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        # snippets = Snippet.objects.all()
        # serializer = SnippetSerializer(snippets, many=True)
        return Response(top20beer(beer_data, 'popularity'))


@api_view(['GET'])
def BeersByrecommendation(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        # snippets = Snippet.objects.all()
        # serializer = SnippetSerializer(snippets, many=True)
        print("starting recommendation engine")
        print("formatted_ratings", formatted_ratings)
        print("formatted_criteriaOrdering",
              formatted_criteriaOrdering)
        print("starting recommendation engine")
        print("formatted_ratings", formatted_ratings)
        print("formatted_criteriaOrdering", formatted_criteriaOrdering)
        rst = recommend(formatted_ratings, formatted_criteriaOrdering)
        print("rst", rst)
        print("generating top 20")
        recommendations = top20result(beer_data, rst)
        print(recommendations)
        # return Response(recommendations)
        # recommendations = [
        #     {
        #         "id": 1,
        #         "name": "Beer 1",
        #         "abv": 4,
        #         "country": "UK",
        #         "style": "Pale Ale 1",
        #         "img": "BeerPictures/beer1.jpg"
        #     },
        #     {
        #         "id": 2,
        #         "name": "Beer 2",
        #         "abv": 5,
        #         "country": "Nepal",
        #         "style": "Pale Ale 3",
        #         "img": "BeerPictures/beer1.jpg"
        #     },
        #     {
        #         "id": 3,
        #         "name": "Beer 3",
        #         "abv": 6,
        #         "country": "Taiwan",
        #         "style": "Pale Ale 2",
        #         "img": "BeerPictures/beer1.jpg"
        #     },
        #     {
        #         "id": 4,
        #         "name": "Beer 4",
        #         "abv": 6,
        #         "country": "Taiwan",
        #         "style": "Pale Ale 2",
        #         "img": "BeerPictures/beer1.jpg"
        #     },
        #     {
        #         "id": 5,
        #         "name": "Beer 5",
        #         "abv": 6,
        #         "country": "Taiwan",
        #         "style": "Pale Ale 2",
        #         "img": "BeerPictures/beer1.jpg"
        #     }
        # ]
        return Response(recommendations)


def top20result(beer_data, rst):
    result = []
    tops = rst

    for idx in tops:
        tmp = beer_reviews[beer_reviews['beer_beerid'] == idx][['beer_beerid', 'beer_name',
                                                                'beer_abv', 'country_name', 'beer_style', 'brewery_name']].fillna(0).drop_duplicates().values
        # print(tmp)
        result.append({
            'id': tmp[0][0],
            'name': tmp[0][1],
            'abv': tmp[0][2],
            'country': tmp[0][3],
            'style': tmp[0][4],
            'brewery': tmp[0][5]
        })
    return result


@api_view(['POST'])
def UserPreferences(request):
    if request.method == 'POST':
        # print(request.data['beerRatings'])
        global formatted_ratings
        global formatted_criteriaOrdering
        formatted_ratings = format_ratings(request.data['beerRatings'])
        # print(formatted_ratings)
        criteria_ordering = request.data["criteriaOrdering"][-1]
        formatted_criteriaOrdering = format_criteriaOrdering(criteria_ordering)

    return Response({"message": "Hello, world!"})


def recommend(inp, criteriaOrdering):
    user_ratings = pd.DataFrame(inp)
    user_ratings['review_profilename'] = 'web_user'
    beer_data_new = beer_data.append(
        user_ratings, sort=False, ignore_index=True)

    review_columns = ["review_taste", "review_palate",
                      "review_appearance", "review_aroma"]
    beer_data_array = dict()

    for col in review_columns:
        col_data = makeData(beer_data_new, col)
        beer_data_array[col] = col_data.build_full_trainset()

    fitted = {}

    for criteria, review_data in beer_data_array.items():
        algo = SVD()
        trainset = review_data
        fitted[criteria] = algo.fit(trainset)

    iids = beer_data_new['beer_beerid'].unique()
    iidsuser = beer_data_new.loc[beer_data_new['review_profilename']
                                 == 'web_user', 'beer_beerid']
    iids_to_predict = np.setdiff1d(iids, iidsuser)

    testset = [['web_user', iid, 4.] for iid in iids_to_predict]
    predictions = []

    for criteria, review_data in beer_data_array.items():
        algo = fitted[criteria]
        crit_preds = algo.test(testset)
        pred_df = pd.DataFrame(crit_preds)
        pred_df = pred_df.set_index(["uid", "iid"])
        pred_df = pred_df.sort_index()
        pred_df = pred_df["est"]
        predictions.append(pred_df)

    pred_df = pd.concat(predictions, axis=1)

    user_pref = np.array(criteriaOrdering)
    preds = {}

    for test_item in testset:
        curr_user = test_item[0]
        curr_beer = test_item[1]
        actual_score = test_item[2]

        criteria_scores = pred_df.loc[(curr_user, curr_beer)]
        pred = np.dot(user_pref, criteria_scores)

        preds[curr_beer] = pred

    return pd.Series(preds).sort_values(ascending=False).head(20).index.tolist()


def format_ratings(rating_array):
    new_rating_arr = []
    for rating in rating_array:
        filtered_objs = [d.get('beer_beerid', None) == rating["id"]
                         for d in new_rating_arr]
        if not any(filtered_objs):
            new_rating_arr.append({"beer_beerid": rating["id"], "review_appearance": 0,
                                   "review_taste": 0, "review_palate": 0, "review_aroma": 0, "review_overall": 0})
        curr_obj = [idx for idx, d in enumerate(
            new_rating_arr) if d["beer_beerid"] == rating["id"]][0]
        if(rating["criteria"] == "appearance"):
            new_rating_arr[curr_obj]["review_appearance"] = rating["rating"]
        elif rating["criteria"] == "palate":
            new_rating_arr[curr_obj]["review_criteria"] = rating["rating"]
        elif rating["criteria"] == "aroma":
            new_rating_arr[curr_obj]["review_aroma"] = rating["rating"]
        elif rating["criteria"] == "taste":
            new_rating_arr[curr_obj]["review_taste"] = rating["rating"]
        elif rating["criteria"] == "overall":
            new_rating_arr[curr_obj]["overall"] = rating["rating"]

    return new_rating_arr


def format_criteriaOrdering(criteriaOrdering):
    weights = [0.4, 0.3, 0.2, 0.1]
    for idx, item in enumerate(criteriaOrdering):
        item["weight"] = weights[idx]
    taste_idx = [idx for idx, d in enumerate(
        criteriaOrdering) if d["type"] == "Taste"][0]
    palate_idx = [idx for idx, d in enumerate(
        criteriaOrdering) if d["type"] == "Palate"][0]
    appearance_idx = [idx for idx, d in enumerate(
        criteriaOrdering) if d["type"] == "Appearance"][0]
    aroma_idx = [idx for idx, d in enumerate(
        criteriaOrdering) if d["type"] == "Aroma"][0]
    formatted_ordering = [criteriaOrdering[taste_idx]['weight'], criteriaOrdering[palate_idx]
                          ['weight'], criteriaOrdering[appearance_idx]['weight'], criteriaOrdering[aroma_idx]['weight']]
    return formatted_ordering

# def format_ratings(rating_array):
#     rating_dict = {}
#     for rating in rating_array:
#         if not rating["id"] in rating_dict:
#             rating_dict[rating["id"]] = {
#                 "appearance": 0, "taste": 0,  "palate": 0, "aroma": 0, "overall": 0}
#         rating_dict[rating["id"]][rating["criteria"]] = rating["rating"]
#     return rating_dict
