import React from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// components / actions
import BeerDetailPage from "../pages/DetailViewPage";
import * as beerActions from "../actions";

const DetailViewContainer = ({
  selectedBeer,
  beersByRating,
  beersByPopularity,
  ratingsForBeer,
  actions
}) =>
  beersByPopularity.length > 0 ? (
    <BeerDetailPage
      selectedBeerObj={
        beersByRating.find(x => x.id === selectedBeer) != null
          ? beersByRating.find(x => x.id === selectedBeer)
          : beersByPopularity.find(x => x.id === selectedBeer)
      }
      setRating={actions.setRating}
      ratings={ratingsForBeer.filter(x => x.id === selectedBeer)}
    />
  ) : (
    <p>Select Some Beer to view details here</p>
  );

DetailViewContainer.propTypes = {
  beersByRating: PropTypes.arrayOf(PropTypes.object).isRequired,
  beersByPopularity: PropTypes.arrayOf(PropTypes.object).isRequired,
  selectedBeer: PropTypes.number.isRequired,
  ratingsForBeer: PropTypes.arrayOf(PropTypes.object).isRequired,
  actions: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  beersByRating: state.beersReducer.beersByRating,
  beersByPopularity: state.beersReducer.beersByPopularity,
  selectedBeer: state.beersReducer.selectedBeer,
  ratingsForBeer: state.beersReducer.ratingsForBeer,
  actions: PropTypes.object.isRequired
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, beerActions), dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailViewContainer);
