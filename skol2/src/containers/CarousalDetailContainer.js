import React from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import CarousalDetailPage from "../pages/CarousalDetailPage";
import * as beerActions from "../actions";

const CarousalDetailContainer = ({
  recommendedBeers,
  selectedRecommendedBeer
}) =>
  recommendedBeers.length > 0 && selectedRecommendedBeer != null ? (
    <CarousalDetailPage
      selectedBeerObj={recommendedBeers.find(
        x => x.id === selectedRecommendedBeer
      )}
    />
  ) : (
    <p>Select Some Beer to view details here</p>
  );

CarousalDetailContainer.propTypes = {
  recommendedBeers: PropTypes.arrayOf(PropTypes.object).isRequired,
  selectedRecommendedBeer: PropTypes.number.isRequired
};

const mapStateToProps = state => ({
  recommendedBeers: state.beersReducer.recommendedBeers,
  selectedRecommendedBeer: state.beersReducer.selectedRecommendedBeer
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, beerActions), dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CarousalDetailContainer);
