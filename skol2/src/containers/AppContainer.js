// function to create initial store setup
import React from "react";
import { Provider } from "react-redux";
import logger from "redux-logger";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import Welcome from "react-welcome-page";
import HorizontalLinearStepper from "../components/stepper";
import { Container, Row, Col } from "react-bootstrap";

// components / reducers
import AppLayout from "../layouts/AppLayout";
import rootReducer from "../reducers";

const middleware = [logger, thunk];

const configureStore = initialState => {
  const enhancers = applyMiddleware(...middleware);
  return createStore(rootReducer, initialState, composeWithDevTools(enhancers));
};

// create store
const store = configureStore({});

// wrap rest of the App in a provider
const AppContainer = () => (
  <Provider store={store}>
    <Container>
      <Row>
        <Welcome
          loopDuration={500}
          data={[
            {
              backgroundColor: "rgb(251, 251, 251)",
              image: require("../skollogo.png"),
              text: "SKOL! Let's drink beer.",
              textAnimation: "zoomIn"
            }
          ]}
        />
        <HorizontalLinearStepper />
      </Row>
    </Container>
  </Provider>
);

export default AppContainer;
