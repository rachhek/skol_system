import React, { Component, Fragment } from "react";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Carousal } from "../components/Carousal";

import * as beerActions from "../actions";
import { selectRecoBeer, getBeersByRecommendation } from "../actions";
export class CarousalContainer extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    recommendedBeers: PropTypes.arrayOf(PropTypes.object).isRequired,
    selectedRecommendedBeer: PropTypes.number.isRequired,
    getBeersByRecommendation: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.getBeersByRecommendation();
  }
  render() {
    return (
      <div>
        {this.props.recommendedBeers.length > 0 ? (
          <Carousal
            recommendedBeers={this.props.recommendedBeers}
            selectedRecommendedBeer={this.props.selectedRecommendedBeer}
            selectRecoBeer={this.props.selectRecoBeer}
          />
        ) : (
          <div>No Beers</div>
        )}
      </div>
    );
  }
}

// const CarousalContainer = ({
//   recommendedBeers,
//   selectedRecommendedBeer,
//   actions
// }) => (
//   //   <button onClick={actions.orderCriteria}>A</button>
//   <div>
//     {recommendedBeers.length > 0 ? (
//       <Carousal
//         recommendedBeers={recommendedBeers}
//         selectedRecommendedBeer={selectedRecommendedBeer}
//         selectRecoBeer={actions.selectRecoBeer}
//       />
//     ) : (
//       <div>No Beers</div>
//     )}
//   </div>
// );

// CarousalContainer.propTypes = {
//   actions: PropTypes.object.isRequired,
//   recommendedBeers: PropTypes.arrayOf(PropTypes.object).isRequired,
//   selectedRecommendedBeer: PropTypes.number.isRequired
// };

const mapStateToProps = state => ({
  actions: PropTypes.object.isRequired,
  recommendedBeers: state.beersReducer.recommendedBeers,
  selectedRecommendedBeer: state.beersReducer.selectedRecommendedBeer
});

// const mapDispatchToProps = dispatch => ({
//   actions: bindActionCreators(Object.assign({}, beerActions), dispatch)
// });

export default connect(
  mapStateToProps,
  { getBeersByRecommendation, selectRecoBeer }
)(CarousalContainer);
