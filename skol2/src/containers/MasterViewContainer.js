import React, { Component, Fragment } from "react";

import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Tabs, Tab } from "react-bootstrap";

// components / actions
import MasterViewPage from "../pages/MasterViewPage";
// import * as beerActions from "../actions";
import {
  getBeersByPopularity,
  selectBeer,
  getBeersByRatings
} from "../actions";

export class MasterViewContainer extends Component {
  static propTypes = {
    beersByRating: PropTypes.arrayOf(PropTypes.object).isRequired,
    beersByPopularity: PropTypes.arrayOf(PropTypes.object).isRequired,
    selectedBeer: PropTypes.number.isRequired,
    actions: PropTypes.object.isRequired,
    getBeersByPopularity: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.getBeersByPopularity();
    this.props.getBeersByRatings();
    this.props.selectBeer(2093);
    // this.props.selectBeer(this.props.beersByRating[0].id);
  }

  render() {
    return (
      <Tabs defaultActiveKey="popularity" id="uncontrolled-tab-example">
        <Tab eventKey="popularity" title="By Popularity">
          <div>
            {this.props.beersByPopularity.length > 0 ? (
              <MasterViewPage
                beers={this.props.beersByPopularity}
                selectedBeer={this.props.selectedBeer}
                selectBeer={this.props.selectBeer}
              />
            ) : (
              <div>Loading Beers...</div>
            )}
          </div>
        </Tab>
        <Tab eventKey="rating" title="By Rating">
          {this.props.beersByRating.length > 0 ? (
            <MasterViewPage
              beers={this.props.beersByRating}
              selectedBeer={this.props.selectedBeer}
              selectBeer={this.props.selectBeer}
            />
          ) : (
            <div>Loading Beers...</div>
          )}
        </Tab>
        {/* <Tab eventKey="country" title="By Country">
    <BeerTable rows={beersByPopularity} />
  </Tab> */}
      </Tabs>
    );
  }
}

// const MasterViewContainer = ({
//   beersByRating,
//   beersByPopularity,
//   selectedBeer,
//   actions
// }) => (
//   <Tabs defaultActiveKey="popularity" id="uncontrolled-tab-example">
//     <Tab eventKey="popularity" title="By Popularity">
//       <div>
//         {beersByRating.length > 0 ? (
//           <MasterViewPage
//             beers={actions.getBeersByPopularity()}
//             selectedBeer={selectedBeer}
//             selectBeer={actions.selectBeer}
//           />
//         ) : (
//           <div>Loading Beers...</div>
//         )}
//       </div>
//     </Tab>
//     <Tab eventKey="rating" title="By Rating">
//       {beersByPopularity.length > 0 ? (
//         <MasterViewPage
//           beers={beersByPopularity}
//           selectedBeer={selectedBeer}
//           selectBeer={actions.selectBeer}
//         />
//       ) : (
//         <div>Loading Beers...</div>
//       )}
//     </Tab>
//     {/* <Tab eventKey="country" title="By Country">
//     <BeerTable rows={beersByPopularity} />
//   </Tab> */}
//   </Tabs>
// );

// MasterViewContainer.propTypes = {
//   beersByRating: PropTypes.arrayOf(PropTypes.object).isRequired,
//   beersByPopularity: PropTypes.arrayOf(PropTypes.object).isRequired,
//   selectedBeer: PropTypes.number.isRequired,
//   actions: PropTypes.object.isRequired,
//   getBeersByPopularity: PropTypes.func.isRequired
// };

const mapStateToProps = state => ({
  beersByRating: state.beersReducer.beersByRating,
  beersByPopularity: state.beersReducer.beersByPopularity,
  selectedBeer: state.beersReducer.selectedBeer
});

// const mapDispatchToProps = dispatch => ({
//   actions: bindActionCreators(Object.assign({}, beerActions), dispatch)
// });

export default connect(
  mapStateToProps,
  { getBeersByPopularity, selectBeer, getBeersByRatings }
)(MasterViewContainer);
