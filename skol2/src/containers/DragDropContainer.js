import React from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { DragDrop } from "../components/Dragdrop";

import BeerDetailPage from "../pages/DetailViewPage";
import * as beerActions from "../actions";

const DrapDropContainer = ({ actions }) => (
  //   <button onClick={actions.orderCriteria}>A</button>
  <DragDrop ignite={actions.orderCriteria} />
);

DrapDropContainer.propTypes = {
  actions: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  actions: PropTypes.object.isRequired
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, beerActions), dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DrapDropContainer);
