import React from "react";
import PropTypes from "prop-types";

const ListViewItem = ({ name, active, handleOnClick }) => (
  <tr className={`${active && "active-list-item"}`} onClick={handleOnClick}>
    <td className="td-height">{name}</td>
  </tr>
);

ListViewItem.propTypes = {
  name: PropTypes.string.isRequired,
  active: PropTypes.bool.isRequired,
  handleOnClick: PropTypes.func.isRequired
};

export default ListViewItem;
