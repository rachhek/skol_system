import Ratings from "react-ratings-declarative";
import React, { Component } from "react";
import { setRating } from "../actions/index";
import { connect } from "react-redux";

export class Rating extends Component {
  state = {
    rating: 0
  };

  changeRating(newRating) {
    this.setState({
      rating: newRating
    });
    this.props.setRating(
      this.props.selectedBeer,
      this.props.criteria,
      newRating
    );
  }

  render() {
    const { widgetColor, initialRating } = this.props;
    this.changeRating = this.changeRating.bind(this);
    return (
      <Ratings
        rating={this.state.rating}
        widgetRatedColors={widgetColor ? widgetColor : "red"}
        widgetDimensions="30px"
        rating={initialRating}
        changeRating={this.changeRating}
      >
        <Ratings.Widget widgetHoverColor={widgetColor ? widgetColor : "red"} />
        <Ratings.Widget widgetHoverColor={widgetColor ? widgetColor : "red"} />
        <Ratings.Widget widgetHoverColor={widgetColor ? widgetColor : "red"} />
        <Ratings.Widget widgetHoverColor={widgetColor ? widgetColor : "red"} />
        <Ratings.Widget widgetHoverColor={widgetColor ? widgetColor : "red"} />
      </Ratings>
    );
  }
}

const mapStateToProps = state => ({
  beersByRating: state.beersReducer.beersByRating,
  selectedBeer: state.beersReducer.selectedBeer
});

export default connect(
  mapStateToProps,
  { setRating }
)(Rating);
