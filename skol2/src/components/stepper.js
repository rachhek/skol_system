import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import AppLayout from "../layouts/AppLayout";
import AppLayout2 from "../layouts/AppLayout2";
import AppLayout3 from "../layouts/AppLayout3";
import { postUserPreferences, setFirstStepSkipped } from "../actions";
import { connect } from "react-redux";
import compose from "recompose/compose";

const styles = theme => ({
  root: {
    width: "100%"
  },
  button: {
    marginRight: theme.spacing.unit
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit
  }
});

function getSteps() {
  return ["Rate Some Beer", "Adjust Preference", "Get Recommendation"];
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return <AppLayout />;
    case 1:
      return <AppLayout2 />;
    case 2:
      return <AppLayout3 />;
    default:
      return "Unknown step";
  }
}

class HorizontalLinearStepper extends React.Component {
  static propTypes = {
    postUserPreferences: PropTypes.func.isRequired,
    ratingsForBeer: PropTypes.arrayOf(PropTypes.object).isRequired,
    criteriaOrdering: PropTypes.arrayOf(PropTypes.object).isRequired,
    setFirstStepSkipped: PropTypes.func.isRequired
  };
  state = {
    activeStep: 0,
    skipped: new Set()
  };

  isStepOptional = step => step === 0;

  handleNext = () => {
    const { activeStep } = this.state;
    if (activeStep == 1) {
      var UserData = {
        beerRatings: this.props.ratingsForBeer,
        criteriaOrdering: this.props.criteriaOrdering,
        firstStepSkipped: this.props.firstStepSkipped
      };
      this.props.postUserPreferences(UserData);
    }
    let { skipped } = this.state;
    if (this.isStepSkipped(activeStep)) {
      skipped = new Set(skipped.values());
      skipped.delete(activeStep);
    }
    this.setState({
      activeStep: activeStep + 1,
      skipped
    });
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1
    }));
  };

  handleSkip = () => {
    const { activeStep } = this.state;
    console.log("skipped hello");
    this.props.setFirstStepSkipped(true);
    if (!this.isStepOptional(activeStep)) {
      // You probably want to guard against something like this,
      // it should never occur unless someone's actively trying to break something.
      throw new Error("You can't skip a step that isn't optional.");
    }

    this.setState(state => {
      const skipped = new Set(state.skipped.values());
      skipped.add(activeStep);
      return {
        activeStep: state.activeStep + 1,
        skipped
      };
    });
  };

  handleReset = () => {
    this.setState({
      activeStep: 0
    });
  };

  isStepSkipped(step) {
    return this.state.skipped.has(step);
  }

  render() {
    const { classes } = this.props;
    const steps = getSteps();
    const { activeStep } = this.state;

    return (
      <div className={classes.root}>
        <Stepper activeStep={activeStep}>
          {steps.map((label, index) => {
            const props = {};
            const labelProps = {};
            if (this.isStepOptional(index)) {
              labelProps.optional = (
                <Typography variant="caption">Optional</Typography>
              );
            }
            if (this.isStepSkipped(index)) {
              props.completed = false;
            }
            return (
              <Step key={label} {...props}>
                <StepLabel {...labelProps}>{label}</StepLabel>
              </Step>
            );
          })}
        </Stepper>
        <div>
          {activeStep === steps.length ? (
            <div>
              <Typography className={classes.instructions}>
                All steps completed - you&apos;re finished
              </Typography>
              <Button onClick={this.handleReset} className={classes.button}>
                Reset
              </Button>
            </div>
          ) : (
            <div>
              <Typography className={classes.instructions}>
                {getStepContent(activeStep)}
              </Typography>
              <div>
                <Button
                  disabled={activeStep === 0}
                  onClick={this.handleBack}
                  className={classes.button}
                >
                  Back
                </Button>
                {this.isStepOptional(activeStep) && (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={this.handleSkip}
                    className={classes.button}
                  >
                    Skip
                  </Button>
                )}
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleNext}
                  className={classes.button}
                >
                  {activeStep === steps.length - 1 ? "Finish" : "Next"}
                </Button>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

HorizontalLinearStepper.propTypes = {
  classes: PropTypes.object
};

const mapStateToProps = state => ({
  ratingsForBeer: state.beersReducer.ratingsForBeer,
  criteriaOrdering: state.beersReducer.criteriaOrdering,
  firstStepSkipped: state.beersReducer.firstStepSkipped
});

export default connect(
  mapStateToProps,
  { postUserPreferences, setFirstStepSkipped }
)(withStyles(styles)(HorizontalLinearStepper));
