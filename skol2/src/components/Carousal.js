import React, { Component } from "react";
import ReactDOM from "react-dom";
import Coverflow from "react-coverflow";

const fn = function() {
  /* do your action */
  console.log("hi");
};
export class Carousal extends Component {
  constructor(props) {
    super(props);

    this.clicked = this.clicked.bind(this);
  }

  clicked(id) {
    console.log("from props");
    this.props.selectRecoBeer(id);
  }

  render() {
    return (
      <Coverflow
        width={720}
        height={500}
        displayQuantityOfSide={2}
        navigation={false}
        enableHeading={true}
        clickable={true}
        infiniteScroll
        active={0}
      >
        {this.props.recommendedBeers.map(beer => (
          <div onClick={param => this.clicked(beer.id)} alt={beer.name}>
            <img
              src={require("../BeerPictures/beer" +
                Math.floor(Math.random() * 10 + 1) +
                ".jpg")}
              alt="title or description"
              style={{ display: "block", width: "100%", height: "100%" }}
            />
          </div>
        ))}
        {/* <div onClick={this.clicked} alt="h">
          <img
            src="https://image.freepik.com/free-vector/pack-colorful-square-emoticons_23-2147589525.jpg"
            alt="title or description"
            style={{ display: "block", width: "100%", height: "100%" }}
          />
        </div>

        <img
          src="https://image.freepik.com/free-vector/pack-colorful-square-emoticons_23-2147589525.jpg"
          alt="title or description"
          style={{ display: "block", width: "100%", height: "100%" }}
        />

        <img
          src="https://image.freepik.com/free-vector/pack-colorful-square-emoticons_23-2147589525.jpg"
          alt="title or description"
          data-action="http://andyyou.github.io/react-coverflow/"
          style={{ display: "block", width: "100%", height: "100%" }}
        /> */}
      </Coverflow>
    );
  }
}
