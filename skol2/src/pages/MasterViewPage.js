import React from "react";
import PropTypes from "prop-types";

import ListViewItem from "../components/ListViewItem";

const BeerMasterPage = ({ beers, selectedBeer, selectBeer }) => (
  <div className="beerTable">
    <table className="table table-hover">
      <tbody>
        {beers.map(beer => (
          <ListViewItem
            key={beer.id}
            name={beer.name}
            active={selectedBeer === beer.id}
            handleOnClick={() => selectBeer(beer.id)}
          />
        ))}
      </tbody>
    </table>
  </div>
);

BeerMasterPage.propTypes = {
  beers: PropTypes.arrayOf(PropTypes.object).isRequired,
  selectedBeer: PropTypes.number.isRequired,
  selectBeer: PropTypes.func.isRequired
};

export default BeerMasterPage;
