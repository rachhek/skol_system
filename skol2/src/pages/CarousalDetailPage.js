import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "react-bootstrap";

const CarousalDetailPage = ({ selectedBeerObj }) => (
  <ul class="list-group list-group-flush">
    <li class="list-group-item">
      <Row className="justify-content-md-center align-items-center">
        <Col md="4">
          <h5 class="card-title">Name</h5>
        </Col>
        <Col md="8">{selectedBeerObj.name}</Col>
      </Row>
    </li>
    <li class="list-group-item">
      <Row className="justify-content-md-center align-items-center">
        <Col md="4">
          <h5 class="card-title">Style</h5>
        </Col>
        <Col md="8">{selectedBeerObj.style}</Col>
      </Row>
    </li>
    <li class="list-group-item">
      <Row className="justify-content-md-center align-items-center">
        <Col md="4">
          <h5 class="card-title">Abv</h5>
        </Col>
        <Col md="8">{selectedBeerObj.abv}</Col>
      </Row>
    </li>
    <li class="list-group-item">
      <Row className="justify-content-md-center align-items-center">
        <Col md="4">
          <h5 class="card-title">Country</h5>
        </Col>
        <Col md="8">{selectedBeerObj.country}</Col>
      </Row>
    </li>
  </ul>
);

export default CarousalDetailPage;
