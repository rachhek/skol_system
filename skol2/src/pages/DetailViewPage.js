import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "react-bootstrap";
import Rating from "../components/Ratings";

const BeerDetailPage = ({ selectedBeerObj, ratings }) => (
  <ul class="list-group list-group-flush">
    <li class="list-group-item">
      <Row className="justify-content-md-center align-items-center">
        <Col md="4">
          <h5 class="card-title">Name</h5>
        </Col>
        <Col md="8">{selectedBeerObj.name}</Col>
      </Row>
    </li>
    <li class="list-group-item">
      <Row className="justify-content-md-center align-items-center">
        <Col md="4">
          <h5 class="card-title">Style</h5>
        </Col>
        <Col md="8">{selectedBeerObj.style}</Col>
      </Row>
    </li>
    <li class="list-group-item">
      <Row className="justify-content-md-center align-items-center">
        <Col md="4">
          <h5 class="card-title">Abv</h5>
        </Col>
        <Col md="8">{selectedBeerObj.abv}</Col>
      </Row>
    </li>
    <li class="list-group-item">
      <Row className="justify-content-md-center align-items-center">
        <Col md="4">
          <h5 class="card-title">Country</h5>
        </Col>
        <Col md="8">{selectedBeerObj.country}</Col>
      </Row>
    </li>
    <li class="list-group-item">
      <Row className="justify-content-md-center align-items-center">
        <Col md="4">
          <h5 class="card-title">Brewery</h5>
        </Col>
        <Col md="8">{selectedBeerObj.brewery}</Col>
      </Row>
    </li>
    <h2 class="card-header">Provide ratings</h2>
    <li class="list-group-item">
      <Row className="justify-content-md-center align-items-center">
        <Col md="4">
          <h5 class="card-subtitle text-muted">Appearance</h5>
        </Col>
        <Col md="8">
          <Rating
            criteria="appearance"
            initialRating={
              ratings.filter(x => x.criteria === "appearance").length > 0
                ? ratings.filter(x => x.criteria === "appearance")[
                    ratings.filter(x => x.criteria === "appearance").length - 1
                  ].rating
                : 0
            }
          />
        </Col>
      </Row>
    </li>
    <li class="list-group-item">
      <Row className="justify-content-md-center align-items-center">
        <Col md="4">
          <h5 class="card-subtitle text-muted">Taste</h5>
        </Col>
        <Col md="8">
          <Rating
            criteria="taste"
            initialRating={
              ratings.filter(x => x.criteria === "taste").length > 0
                ? ratings.filter(x => x.criteria === "taste")[
                    ratings.filter(x => x.criteria === "taste").length - 1
                  ].rating
                : 0
            }
          />
        </Col>
      </Row>
    </li>
    <li class="list-group-item">
      <Row className="justify-content-md-center align-items-center">
        <Col md="4">
          <h5 class="card-subtitle text-muted">Palate</h5>
        </Col>
        <Col md="8">
          <Rating
            criteria="palate"
            initialRating={
              ratings.filter(x => x.criteria === "palate").length > 0
                ? ratings.filter(x => x.criteria === "palate")[
                    ratings.filter(x => x.criteria === "palate").length - 1
                  ].rating
                : 0
            }
          />
        </Col>
      </Row>
    </li>
    <li class="list-group-item">
      <Row className="justify-content-md-center align-items-center">
        <Col md="4">
          <h5 class="card-subtitle text-muted">Aroma</h5>
        </Col>
        <Col md="8">
          <Rating
            criteria="aroma"
            initialRating={
              ratings.filter(x => x.criteria === "aroma").length > 0
                ? ratings.filter(x => x.criteria === "aroma")[
                    ratings.filter(x => x.criteria === "aroma").length - 1
                  ].rating
                : 0
            }
          />
        </Col>
      </Row>
    </li>
    <li class="list-group-item">
      <Row className="justify-content-md-center align-items-center">
        <Col md="4">
          <h5 class="card-subtitle text-muted">Overall</h5>
        </Col>
        <Col md="8">
          <Rating
            criteria="overall"
            initialRating={
              ratings.filter(x => x.criteria === "overall").length > 0
                ? ratings.filter(x => x.criteria === "overall")[
                    ratings.filter(x => x.criteria === "overall").length - 1
                  ].rating
                : 0
            }
            widgetColor="rgb(212,175,55)"
          />
        </Col>
      </Row>
    </li>
  </ul>
);

BeerDetailPage.propTypes = {
  selectedBeerObj: PropTypes.object.isRequired,
  setRating: PropTypes.func.isRequired,
  ratings: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default BeerDetailPage;
