import * as types from "./types";
import axios from "axios";
export const selectBeer = id => ({
  type: types.SET_SELECTED_BEER,
  id
});

export const setFirstStepSkipped = () => ({
  type: types.SET_FIRST_STEP_SKIPPED_FLAG,
  payload: true
});

export const selectRecoBeer = id => ({
  type: types.SET_SELECTED_RECO_BEER,
  id
});

export const getBeersByPopularity = () => (dispatch, getState) => {
  axios.get("http://127.0.0.1:8000/api/beersbypopularity/").then(res => {
    dispatch({
      type: types.GET_BEERS_BY_POPULARITY,
      payload: res.data
    });
  });
};

export const getBeersByRatings = () => (dispatch, getState) => {
  axios.get("http://127.0.0.1:8000/api/beersbyrating/").then(res => {
    dispatch({
      type: types.GET_BEERS_BY_RATING,
      payload: res.data
    });
  });
};

export const getBeersByRecommendation = () => (dispatch, getState) => {
  axios.get("http://127.0.0.1:8000/api/beersbyrecommendation/").then(res => {
    dispatch({
      type: types.GET_BEERS_BY_RECOMMENDATION,
      payload: res.data
    });
  });
};

export const postUserPreferences = obj => (dispatch, getState) => {
  axios.post("http://127.0.0.1:8000/api/userpreferences/", obj).then(res => {
    console.log("posted");
  });
};

// export const getBeersByPopularity = () => ({
//   type: types.GET_BEERS_BY_POPULARITY,
//   payload: [
//     {
//       a: 1
//     }
//   ]
// });

// export const setRating = (id, criteria, rating) => ({
//   type: types.SET_BEER_RATING,
//   payload: {
//     id: id,
//     criteria: criteria,
//     rating: rating
//   }
// });

export const setRating = (id, criteria, rating) => (dispatch, getState) => {
  dispatch({
    type: types.SET_BEER_RATING,
    payload: {
      id: id,
      criteria: criteria,
      rating: rating
    }
  });
};

export const orderCriteria = orderList => (dispatch, getState) => {
  dispatch({
    type: types.SET_ORDER_CRITERIA,
    payload: orderList
  });
};
