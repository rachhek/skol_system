import React from "react";

// components
import MasterViewContainer from "../containers/MasterViewContainer";
import DetailViewContainer from "../containers/DetailViewContainer";
import { Container, Row, Col } from "react-bootstrap";

const AppLayout = () => (
  <Row>
    <Col xs lg={6}>
      <h3 class="card-header">Select Beers</h3>
      {/* <BeerDisplayTabs /> */}
      <MasterViewContainer />
    </Col>
    <Col xs lg={6}>
      <h3 class="card-header">Beer Details</h3>
      {/* <BeerDetails /> */}
      <DetailViewContainer />
    </Col>
  </Row>
);

export default AppLayout;
