import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import CarousalDetailContainer from "../containers/CarousalDetailContainer";
import CarousalContainer from "../containers/CarousalContainer";

const AppLayout3 = () => (
  <Container>
    <h3 class="card-header">We recommend you have one of these beers</h3>
    <Row className="justify-content-md-center align-items-center">
      <Col xs lg={8} className="align-items-center">
        <CarousalContainer />
      </Col>

      <Col xs lg={4} className="align-items-center">
        <CarousalDetailContainer />
      </Col>
    </Row>
  </Container>
);

export default AppLayout3;
