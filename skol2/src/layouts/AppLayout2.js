import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { DragDrop } from "../components/Dragdrop";
import { connect } from "react-redux";
import { orderCriteria, setRating } from "../actions/index";
import DragDropContainer from "../containers/DragDropContainer";

const AppLayout2 = () => (
  <Container>
    <Row className="justify-content-md-center align-items-center">
      <Col xs lg={6} className="align-items-center">
        <h4 className="align-items-center card-header">
          Drag and Drop to order your preference
        </h4>
      </Col>
    </Row>
    <Row className="justify-content-md-center align-items-center">
      <DragDropContainer />
    </Row>
  </Container>
);

export default AppLayout2;
