import * as types from "../actions/types";

const initialState = {
  beersByRating: [
    // {
    //   id: 1,
    //   name: "Beer 1",
    //   abv: 4,
    //   country: "UK",
    //   style: "Pale Ale 1"
    // },
    // {
    //   id: 2,
    //   name: "Beer 2",
    //   abv: 5,
    //   country: "Nepal",
    //   style: "Pale Ale 3"
    // },
    // {
    //   id: 3,
    //   name: "Beer 3",
    //   abv: 6,
    //   country: "Taiwan",
    //   style: "Pale Ale 2"
    // }
  ],
  beersByPopularity: [
    // {
    //   id: 4,
    //   name: "Beer 4",
    //   abv: 4,
    //   country: "UK",
    //   style: "Pale Ale 1"
    // },
    // {
    //   id: 5,
    //   name: "Beer 5",
    //   abv: 5,
    //   country: "Nepal",
    //   style: "Pale Ale 3"
    // },
    // {
    //   id: 6,
    //   name: "Beer 6",
    //   abv: 6,
    //   country: "Taiwan",
    //   style: "Pale Ale 2"
    // }
  ],
  selectedBeer: null,
  ratingsForBeer: [
    { id: 1, criteria: "appearance", rating: 4 },
    { id: 1, criteria: "palate", rating: 4 },
    { id: 1, criteria: "aroma", rating: 4 }
  ],
  criteriaOrdering: [],
  recommendedBeers: [
    // {
    //   id: 1,
    //   name: "Beer 1",
    //   abv: 4,
    //   country: "UK",
    //   style: "Pale Ale 1",
    //   img: "../BeerPictures/beer1.jpg"
    // },
    // {
    //   id: 2,
    //   name: "Beer 2",
    //   abv: 5,
    //   country: "Nepal",
    //   style: "Pale Ale 3",
    //   img: "../BeerPictures/beer1.jpg"
    // },
    // {
    //   id: 3,
    //   name: "Beer 3",
    //   abv: 6,
    //   country: "Taiwan",
    //   style: "Pale Ale 2",
    //   img: "../BeerPictures/beer1.jpg"
    // }
  ],
  selectedRecommendedBeer: null,
  firstStepSkipped: false
};

export default function beersReducer(state = initialState, action) {
  switch (action.type) {
    case types.SET_SELECTED_BEER:
      return {
        ...state,
        selectedBeer: action.id
      };

    case types.SET_SELECTED_RECO_BEER:
      return {
        ...state,
        selectedRecommendedBeer: action.id
      };

    case types.SET_BEER_RATING:
      return {
        ...state,
        ratingsForBeer: [...state.ratingsForBeer, action.payload]
      };

    case types.SET_ORDER_CRITERIA:
      return {
        ...state,
        criteriaOrdering: [...state.criteriaOrdering, action.payload]
      };

    case types.GET_RECOMMENDED_BEERS:
      return {
        ...state,
        recommendedBeers: [...state.recommendedBeers, action.payload]
      };

    case types.GET_BEERS_BY_POPULARITY:
      return {
        ...state,
        beersByPopularity: action.payload
      };

    case types.GET_BEERS_BY_RATING:
      return {
        ...state,
        beersByRating: action.payload
      };

    case types.GET_BEERS_BY_RECOMMENDATION:
      return {
        ...state,
        recommendedBeers: action.payload
      };

    case types.SET_FIRST_STEP_SKIPPED_FLAG:
      return {
        ...state,
        firstStepSkipped: action.payload
      };

    default:
      return state;
  }
}
