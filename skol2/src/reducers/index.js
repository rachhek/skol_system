import { combineReducers } from "redux";
import beersReducer from "./beersReducer";

const rootReducer = combineReducers({
  beersReducer
});

export default rootReducer;
